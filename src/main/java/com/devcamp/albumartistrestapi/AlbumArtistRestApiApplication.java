package com.devcamp.albumartistrestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AlbumArtistRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(AlbumArtistRestApiApplication.class, args);
	}

}
