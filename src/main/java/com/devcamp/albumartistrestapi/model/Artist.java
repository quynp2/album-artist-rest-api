package com.devcamp.albumartistrestapi.model;

import java.util.ArrayList;

public class Artist {
    private int id;
    private String name;
    private ArrayList<Album> album;

    public Artist() {
    }

    public Artist(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Artist(int id, String name, ArrayList<Album> album) {
        this.id = id;
        this.name = name;
        this.album = album;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Album> getAlbum() {
        return album;
    }

    public void setAlbum(ArrayList<Album> album) {
        this.album = album;
    }

}
