package com.devcamp.albumartistrestapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.albumartistrestapi.model.Artist;
import com.devcamp.albumartistrestapi.service.ArtistService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class ArtistController {
    @Autowired
    private ArtistService artistService;

    @GetMapping("/artists")
    public ArrayList<Artist> getAllArtist() {
        ArrayList<Artist> allArtist = artistService.getAllArtist();
        return allArtist;

    }

    /**
     * @param id
     * @return
     */
    @GetMapping("/artist-info")
    public Artist GetArtistInfo(@RequestParam(required = true, name = "artistId") Integer id) {

        ArrayList<Artist> allArtist = artistService.getAllArtist();
        Artist findArtist = new Artist();
        for (Artist artistElement : allArtist) {
            if (artistElement.getId() == id) {
                findArtist = artistElement;
            }
        }
        return findArtist;

    }

}
