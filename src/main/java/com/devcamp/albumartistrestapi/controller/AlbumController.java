package com.devcamp.albumartistrestapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.albumartistrestapi.model.Album;
import com.devcamp.albumartistrestapi.service.AlbumService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class AlbumController {
    @Autowired
    private AlbumService albumService;

    @GetMapping("/album-info")

    public Album getAlbumInfo(@RequestParam(required = true, name = "albumId") int id) {
        ArrayList<Album> getAllAlbum = albumService.GetAllAlbum();

        Album findAlbum = new Album();
        for (Album album : getAllAlbum) {
            if (album.getId() == id) {
                findAlbum = album;
            }

        }
        return findAlbum;

    }

}
