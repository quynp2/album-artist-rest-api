package com.devcamp.albumartistrestapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.albumartistrestapi.model.Album;

@Service
public class AlbumService {

    Album LSS = new Album(001, "Lan Song Xanh", new String[] { "my tam", "luu huong giang" });
    Album LSD = new Album(2, "Lan Song Do", new String[] { "Ngo Thi Hoang Nghia" });
    Album LSV = new Album(003, "Lan Song Vang", new String[] { "My Tam", "Luu Thien Huong" });

    Album Monster = new Album(004, "Monster", new String[] { "BBB", "Lies" });
    Album BangBang = new Album(005, "Bangbang", new String[] { "Monster", "Fantastic Baby" });
    Album LastDance = new Album(006, "Last dance", new String[] { "last dance", "flower" });

    Album Nhacthieunhi = new Album(7, "Con co be be", new String[] { "Quoc Ca", "Tien Quan Ca" });
    Album Nhacdoanvien = new Album(8, "Nhung bong hoa nho", new String[] { "5ae tren 1 chiec xe tang", "Luu dan" });
    Album Nhacdang = new Album(9, "Dang Viet Nam",
            new String[] { "Binh doan ta di len", "Truong Son Dong Truong Son Tay" });

    public ArrayList<Album> GetAlbumVNHotHit() {
        ArrayList<Album> AlbumVietNamHot = new ArrayList<>();
        AlbumVietNamHot.add(LSS);
        AlbumVietNamHot.add(LSD);
        AlbumVietNamHot.add(LSV);
        return AlbumVietNamHot;
    }

    public ArrayList<Album> GetAlbumKorea() {
        ArrayList<Album> AlbumKoreaHot = new ArrayList<>();
        AlbumKoreaHot.add(Monster);
        AlbumKoreaHot.add(BangBang);
        AlbumKoreaHot.add(LastDance);
        return AlbumKoreaHot;

    }

    public ArrayList<Album> GetAlbumDongLao() {
        ArrayList<Album> AlbumDongLaoHot = new ArrayList<>();
        AlbumDongLaoHot.add(Nhacthieunhi);
        AlbumDongLaoHot.add(Nhacdoanvien);
        AlbumDongLaoHot.add(Nhacdang);
        return AlbumDongLaoHot;

    }

    public ArrayList<Album> GetAllAlbum() {
        ArrayList<Album> allAlbum = new ArrayList<>();
        allAlbum.add(LSS);
        allAlbum.add(LSD);
        allAlbum.add(LSV);
        allAlbum.add(Monster);
        allAlbum.add(BangBang);
        allAlbum.add(LastDance);
        allAlbum.add(Nhacthieunhi);
        allAlbum.add(Nhacdoanvien);
        allAlbum.add(Nhacdang);

        return allAlbum;

    }

}
