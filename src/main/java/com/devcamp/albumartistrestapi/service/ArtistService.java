package com.devcamp.albumartistrestapi.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.albumartistrestapi.model.Artist;

@Service
public class ArtistService {
    @Autowired
    private AlbumService albumService;
    Artist MyTamSinger = new Artist(01, "Nguyen My Tam");
    Artist BigBangGroup = new Artist(02, "Big Bang Member");
    Artist HoangNghiArtist = new Artist(03, "Ngo Thi Hoang Nghia");

    /**
     * @return
     */
    public ArrayList<Artist> getAllArtist() {
        ArrayList<Artist> allArtist = new ArrayList<>();
        MyTamSinger.setAlbum(albumService.GetAlbumVNHotHit());
        BigBangGroup.setAlbum(albumService.GetAlbumKorea());
        HoangNghiArtist.setAlbum(albumService.GetAlbumDongLao());

        allArtist.add(MyTamSinger);
        allArtist.add(BigBangGroup);
        allArtist.add(HoangNghiArtist);
        return allArtist;

    }

}
